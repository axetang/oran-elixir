#!/usr/bin/bash

appmgr_add=`kubectl get svc -n ricplt --field-selector metadata.name=service-ricplt-appmgr-http -o jsonpath='{.items[0].spec.clusterIP}'`

/local/setup/oran/dms_cli uninstall axesxapp --version=0.1.0 --namespace=ricxapp

curl -X 'POST' 'http://'"$appmgr_add"':8080/ric/v1/deregister' -H 'accept: application/json' -H 'Content-Type: application/json' -d '{ 
  "appName": "axesxapp",
  "appInstanceName": "axesxapp"
  }'