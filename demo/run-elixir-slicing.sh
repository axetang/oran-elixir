#!/bin/bash

# For running the script locally on the node.
export XAPP_ADDR=`kubectl get svc -n ricxapp --field-selector metadata.name=service-ricxapp-axesxapp-http -o jsonpath='{.items[0].spec.clusterIP}'`
if [ -z "$XAPP_ADDR" ]; then
    export XAPP_ADDR=`kubectl get svc -n ricxapp --field-selector metadata.name=service-ricxapp-axesxapp-rmr -o jsonpath='{.items[0].spec.clusterIP}'`
fi
if [ -z "$XAPP_ADDR" ]; then
    echo "ERROR: failed to find axesxapp nbi service; aborting!"
    exit 1
fi
# XAPP_ADDR=155.98.38.63
SLEEPINT=2

# sleep $SLEEPINT
# Send subscription request
echo "Sending subscription request" ; echo
curl -X POST http://${XAPP_ADDR}:4000/sendsubrequest
# sleep $SLEEPINT
# First, create enodeB
echo "Creating NodeB (id=1):" ; echo
curl -X POST -H "Content-type: application/json" -d '{"type":"eNB","id":411,"mcc":"001","mnc":"01"}' http://${XAPP_ADDR}:4000/nodebs
sleep $SLEEPINT
# Then create slices
# Slow slice
echo "Creating Slice (name=slow)": ; echo
curl -X POST -H "Content-type: application/json" -d '{"name":"slow","allocation_policy":{"type":"proportional","share":256}}' http://${XAPP_ADDR}:4000/slices
sleep $SLEEPINT
# Fast slice
echo "Creating Slice (name=fast)": ; echo
curl -X POST -H "Content-type: application/json" -d '{"name":"fast","allocation_policy":{"type":"proportional","share":1024}}' http://${XAPP_ADDR}:4000/slices
sleep $SLEEPINT
# bind slices to nodeb
# bind slow slice
echo "Binding Slice to NodeB (name=slow):" ; echo
curl -X POST http://${XAPP_ADDR}:4000/nodebs/enB_macro_001_001_00019B/slices/slow
sleep $SLEEPINT
# bind fast slice
echo "Binding Slice to NodeB (name=fast):" ; echo
curl -X POST http://${XAPP_ADDR}:4000/nodebs/enB_macro_001_001_00019B/slices/fast
sleep $SLEEPINT
# Now creating the UE
echo "Creating Ue (ue=001010123456789)" ; echo
curl -X POST -H "Content-type: application/json" -d '{"imsi":"001010123456789"}' http://${XAPP_ADDR}:4000/ues
sleep $SLEEPINT
# Now bind the UE to the fast slice
echo "Binding Ue to Slice fast (imsi=001010123456789):" ; echo
curl -X POST http://${XAPP_ADDR}:4000/slices/fast/ues/001010123456789
sleep $SLEEPINT

#sleep 10

# Unbind UE from the fast slice
# echo "unbinding Ue to Slice fast" ; echo
# curl -X DELETE http://${XAPP_ADDR}:4000/slices/fast/ues/001010123456789
# sleep $SLEEPINT
# # Bind the UE to the slow slice
# echo "Binding Ue to Slice slow (imsi=001010123456789):" ; echo
# curl -X POST http://${XAPP_ADDR}:4000/slices/slow/ues/001010123456789
# sleep $SLEEPINT

# curl -X POST -H "Content-type: application/json" -d '{"name":"fast","allocation_policy":{"type":"proportional","share":1024, "throttle":"true", "throttle_period":1800, "throttle_target": 500000}}' http://localhost:4000/slices