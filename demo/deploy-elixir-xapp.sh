#!/usr/bin/bash

/local/setup/oran/dms_cli onboard ~/rmr-side-cart/mock_config.json /local/setup/oran/xapp-embedded-schema.json
/local/setup/oran/dms_cli install --xapp_chart_name=axesxapp --version=0.1.0 --namespace=ricxapp
appmgr_add=`kubectl get svc -n ricplt --field-selector metadata.name=service-ricplt-appmgr-http -o jsonpath='{.items[0].spec.clusterIP}'`
rmr_add=`kubectl get svc -n ricxapp --field-selector metadata.name=service-ricxapp-axesxapp-rmr -o jsonpath='{.items[0].spec.clusterIP}'`


curl -X 'POST' 'http://'"$appmgr_add"':8080/ric/v1/register' -H 'accept: application/json' -H 'Content-Type: application/json' -d '{ 
  "appName": "axesxapp",
  "appVersion": "0.1.0",
  "appInstanceName": "axesxapp",
  "httpEndpoint": "",
  "rmrEndpoint": "'"'$rmr_add'"':4560",
  "config": "{\n    \"containers\": [\n        {\n            \"image\": {\n\t\t        \"registry\": \"gitlab.flux.utah.edu:4567\",\n                \"name\": \"axetang/rmr-side-cart\",\n                \"tag\": \"latest\"\n            },\n            \"name\": \"axesxapp\"\n        }\n    ],\n    \"json_url\": \"axesxapp\",\n    \"messaging\": {\n        \"ports\": [\n            {\n                \"container\": \"axesxapp\",\n                \"description\": \"axesxapp container\",\n                \"name\": \"rmr-data\",\n                \"policies\": [\n                    1\n                ],\n                \"port\": 4560,\n                \"rxMessages\": [\n                    \"RIC_SUB_RESP\",\n                    \"RIC_SUB_FAILURE\",\n                    \"RIC_INDICATION\",\n                    \"RIC_SUB_DEL_RESP\",\n                    \"RIC_SUB_DEL_FAILURE\",\n                    \"RIC_CONTROL_ACK\",\n                    \"RIC_CONTROL_FAILURE\"\n                ],\n                \"txMessages\": [\n                    \"RIC_SUB_REQ\",\n                    \"RIC_SUB_DEL_REQ\",\n                    \"RIC_CONTROL_REQ\"\n                ]\n            },\n            {\n                \"container\": \"axesxapp\",\n                \"description\": \"rmr route port for axesxapp\",\n                \"name\": \"rmr-route\",\n                \"port\": 4561\n            },\n            {\n                \"container\": \"axesxapp\",\n                \"description\": \"http channel\",\n                \"name\": \"http\",\n                \"port\": 4000\n            }\n        ]\n    },\n    \"rmr\": {\n        \"maxSize\": 2072,\n        \"numWorkers\": 1,\n        \"policies\": [\n            1\n        ],\n        \"protPort\": \"tcp:4560\",\n        \"rxMessages\": [\n            \"RIC_SUB_RESP\",\n            \"RIC_SUB_FAILURE\",\n            \"RIC_INDICATION\",\n            \"RIC_SUB_DEL_RESP\",\n            \"RIC_SUB_DEL_FAILURE\",\n            \"RIC_CONTROL_ACK\",\n            \"RIC_CONTROL_FAILURE\"\n        ],\n        \"txMessages\": [\n            \"RIC_SUB_REQ\",\n            \"RIC_SUB_DEL_REQ\",\n            \"RIC_CONTROL_REQ\"\n        ]\n    },\n    \"version\": \"0.1.0\",\n    \"xapp_name\": \"axesxapp\"\n}"
}'

# Patching the kubernetes service so that an external IP is exposed.
host_ip=`hostname --ip-address`
kubectl patch -n ricxapp svc service-ricxapp-axesxapp-http -p '{"spec":{"externalIPs":["'"$host_ip"'"]}}'